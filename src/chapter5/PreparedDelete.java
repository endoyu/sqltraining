package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class PreparedDelete {
	public static void main(String[] args) throws Exception {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/sqltraining";
		String user = "root";
		String password = "tdvv3463";

		Class.forName(driver);

		Connection connection = null;

		try {
			connection = DriverManager.getConnection(url, user, password);
			connection.setAutoCommit(false);

			String deleteId = "1 OR id = id";

			delete(connection, deleteId);

			Select.select(connection);

			connection.commit();
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	public static void delete(Connection connection, String deleteId)
			throws SQLException {
		String sql = "DELETE FROM authors WHERE id = ?";

		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setObject(1,  deleteId, Types.INTEGER);

		int updateCount = statement.executeUpdate();

		if (updateCount == 1) {
			System.out.println("success!");
		} else {
			System.out.println("failure!");
		}

		statement.close();
	}
}
